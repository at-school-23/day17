package ru.devray.day17;

import io.qameta.allure.Step;

public class FakePageObject {

    @Step("Ввести логин '{login}'")
    public void setLogin(String login) {}

    @Step("Ввести пароль '{0}'")
    public void setPassword(String password) {}

    @Step("Войти как пользователь {user.login}")
    public void loginAsUser(User user) {

    }

    @Step("Кликнуть кнопку 'Войти'")
    public void clickLoginButton() {}

    @Step("Войти в настройки профиля")
    public void clickMyProfileButton() {}

    @Step("Установить дату рождения {0}-{1}-{2}")
    public void setDateOfBirth(int day, int month, int year) {}

    @Step("Кликнуть по последнему купленному товару с именем '{wareName}'")
    public void clickWareFromRecommendedWaresByWareName(String wareName) {}


}
