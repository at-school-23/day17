package ru.devray.day17;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;
import ru.devray.day17.pages.MainPage;
import ru.devray.day17.pages.PageObjectSupplier;

public class MvnRepoTest implements PageObjectSupplier {

    @Test
    public void test() {
        mainPage()
                .open()
                .search("junit");
        searchResultsPage()
                .clickSearchResultByIndex(2);
        dependencyPage()
                .assertDependencyDescriptionTextPresent();
    }
}
