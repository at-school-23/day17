package ru.devray.day17;

/*
""
"{0}"
"{parameterName}"
"{parameterName.size}"
 */
public class User {
    public String login;
    public String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
               "login='" + login + '\'' +
               ", password='" + password + '\'' +
               '}';
    }
}
