package ru.devray.day17;

import io.qameta.allure.*;
import io.qameta.allure.testng.Tag;
import io.qameta.allure.testng.Tags;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NotVerySimpleTest {

    @BeforeMethod
    public void setUp() {

    }

    @AllureId("34555")
    @Epic("Карточные платежи")
    @Feature("Прием платежей картами МИР")
    @Story("Прием оплаты с карты МИР индивидуального предпринимателя")
    @Epics({
            @Epic("Карточные платежи"),
            @Epic("Новое поколение фронта"),
            @Epic("СБП"),
    })
    @Tags(
            {@Tag("регресс"), @Tag("Подключение к БД"), @Tag("Платежи")}
    )
    @Link(name = "Confluence", url = "https://ya.ru")
    @Link(name = "Confluence2", url = "https://ya.ru")
//    @TmsLink()
//    @Issue()
    @Test
    public void test() {
        User user = new User("simpleuser", "734875");


        FakePageObject fakePageObject = new FakePageObject();
        fakePageObject.setLogin("vasya");
        fakePageObject.setPassword("123!@#abc");
        fakePageObject.loginAsUser(user);
        fakePageObject.clickLoginButton();
        fakePageObject.clickMyProfileButton();
        fakePageObject.setDateOfBirth(4,8,1991);
        fakePageObject.clickWareFromRecommendedWaresByWareName("успокоительные");
    }

    @AfterMethod
    public void tearDown() {

    }
}
