package ru.devray.day17;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleTest {

    @Test
    public void test1() {
        int result = 2 + 2;

        Assert.assertEquals(result, 4);
    }

    @Test
    public void test2() {
        int result = 2 + 3;

        Assert.assertEquals(result, 5);
    }

    @Test
    public void test3() {
        int result = 2 + 3;

        Assert.assertEquals(result, 4);
    }

    @Test
    public void test4() {
        throw new RuntimeException();
    }

    @Test(enabled = false)
    public void test5() {
        throw new RuntimeException();
    }
}
