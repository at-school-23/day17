package ru.devray.day17.pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$$x;

public class SearchResultsPage {

    ElementsCollection searchResults = $$x("//div[@class='content']/div[@class='im']//h2");

    @Step("Кликнуть по поисковому результату по номеру [{0}]")
    public void clickSearchResultByIndex(int index) {
        searchResults.get(index-1).click();
    }

}
