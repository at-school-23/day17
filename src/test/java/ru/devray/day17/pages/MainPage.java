package ru.devray.day17.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;

//Fluent / Chain of invocations
public class MainPage {

    SelenideElement searchField = $x("//input[@name='q']");
    SelenideElement searchButton = $x("//input[@value='Search']");

    @Step("Открыть главную страницу")
    public MainPage open() {
        Selenide.open("https://mvnrepository.com/");
        return this;
    }

    @Step("Задать поисковый запрос '{searchQuery}'")
    public MainPage setSearchField(String searchQuery) {
        searchField.sendKeys(searchQuery);
        return this;
    }

    @Step("Кликнуть кнопку 'Search'")
    public void clickSearchButton() {
        searchButton.click();
    }

    @Step("Найти зависимость по запросу: '{0}'")
    public void search(String searchQuery) {
        setSearchField(searchQuery);
        clickSearchButton();
    }


}
