package ru.devray.day17.pages;

public interface PageObjectSupplier {

    default MainPage mainPage() { return new MainPage(); }
    default SearchResultsPage searchResultsPage() { return new SearchResultsPage(); }
    default DependecyPage dependencyPage() { return new DependecyPage(); }
}
