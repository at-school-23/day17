package ru.devray.day17.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class DependecyPage {

    SelenideElement depedencyDescriptionLabel = $x("//div[@class='im-description']");

    @Step("Проверить присутствие текста краткого описания зависимости")
    public DependecyPage assertDependencyDescriptionTextPresent() {
        depedencyDescriptionLabel
                .shouldBe(visible)
                .shouldHave(text("\nJUnit is a unit testing framework to write and run repeatable automated tests on Java."));
        return this;
    }

}
